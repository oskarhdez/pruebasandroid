package com.example.pruebasandroid;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_main);

        TextView username=(TextView) findViewById(R.id.username);
        TextView pwd=(TextView) findViewById(R.id.pwd);

        MaterialButton loginbtn=(MaterialButton) findViewById(R.id.loginbtn);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().equals("admin")&&pwd.getText().toString().equals("admin")) {
                    Toast.makeText(MainActivity.this, "usuario registrado", Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(MainActivity.this, "usuario no registrado", Toast.LENGTH_SHORT).show();
            }
        });
    }
}